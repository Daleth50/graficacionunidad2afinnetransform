package proyecto;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class cEscalar extends JDialog
{
	JLabel etiq;
	JTextField ct;
	JButton Ac, Ca;
	float res = 0.0f;
	boolean es;

	public cEscalar(JFrame f, boolean modal, String tit, boolean esc)
	{
		super(f, modal);
		es = esc;
		setLayout(new FlowLayout(FlowLayout.LEFT));
		setSize(500, 90);
		setLocationRelativeTo(f);
		etiq = new JLabel(tit);
		ct = new JTextField(5);
		Ac = new JButton("Aceptar");
		Ca = new JButton("Cancelar");
		add(etiq);
		add(ct);
		add(Ac);
		add(Ca);
		Ac.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				String v = ct.getText();
				if (!v.equals(""))
				{
					res = Float.parseFloat(v);
				} else
				{
					if (es)
						res = 1.0f;
					else
						res = 0.0f;

				}
				setVisible(false);
				dispose();
			}
		});

		Ca.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				res = -1;
				setVisible(false);
				dispose();
			}
		});
		setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
	}

	public float mostrar()
	{
		setVisible(true);
		return res;
	}
}
