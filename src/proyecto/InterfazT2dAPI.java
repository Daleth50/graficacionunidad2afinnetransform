package proyecto;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.event.WindowEvent;
import java.net.URL;

import javax.swing.*;

class raton2 extends MouseMotionAdapter {
	InterfazT2dAPI r;

	public raton2(InterfazT2dAPI ref) {
		r = ref;
	}

	public void mouseDragged(MouseEvent e) {
		if (r.mover) {
			int cx = e.getX();
			int cy = e.getY();
			int fcx = (int) r.F.s.getBounds2D().getCenterX();
			int fcy = (int) r.F.s.getBounds2D().getCenterY();
			r.F.Trasladar(cx - fcx, cy - fcy);
			r.repaint();
		}
	}
}

class raton1 implements MouseListener {
	InterfazT2dAPI r;

	public raton1(InterfazT2dAPI ref) {
		r = ref;
	}

	public void mousePressed(MouseEvent e) {
		if (e.getButton() == MouseEvent.BUTTON3) {
			r.muestraMenu(e);
		} else {
			r.ocultaMenu(e);
		}
		if (e.getClickCount() == 1) {
			int cx = e.getX();
			int cy = e.getY();
			if (r.F.s.contains(cx, cy)) {
				r.mover = true;
			} else {
				r.mover = false;
			}
		} else {
			int cx = e.getX();
			int cy = e.getY();
			if (cx < r.F.s.getBounds().getMinX()) {
				r.F.Rotar(5);
				r.repaint();
			}
			if (cx > r.F.s.getBounds().getMaxX()) {
				r.F.Rotar(-5);
				r.repaint();
			}
		}

	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}
}

public class InterfazT2dAPI extends JPanel implements MouseWheelListener {
	JFrame v;
	Figura2dAPI F;
	JButton b1, b2, b3, b4, b5;
	boolean mover;
	JMenuBar barraM;
	JMenu op1, op2, op3;
	JMenuItem m1, m2, m3, m4, m5, m6, m7;
	JMenuItem vP1, vP2, vP3, vP4, vP5, vP6, vP7;
	JMenuItem ayuda, restaurar;
	JToolBar barraH;
	Action a1, a2, a3, a4;
	Image fondo;
	JPopupMenu popup;
	int vPfig[][];

	public void ponerMenu() {
		barraM = new JMenuBar();
		v.setJMenuBar(barraM);
		op1 = new JMenu("Transformaciones");
		op2 = new JMenu("Vista previa");
		op3 = new JMenu("Ayuda");
		barraM.add(op1);
		barraM.add(op2);
		barraM.add(op3);

		m1 = new JMenuItem("Restaurar");
		m1.setMnemonic('R');
		m1.setToolTipText("Restaura a la figura original");
		m1.setAccelerator(KeyStroke.getKeyStroke('R', InputEvent.CTRL_MASK));
		URL ruta = getClass().getResource("/proyecto/rec/restaurar.png");
		m1.setIcon(new ImageIcon(ruta));
		m1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				F.Restaurar();
				repaint();
			}
		});
		m2 = new JMenuItem("Escalar");
		m2.setMnemonic('E');
		m2.setToolTipText("Escala la figura la cantidad especificada");
		m2.setAccelerator(KeyStroke.getKeyStroke('E', InputEvent.CTRL_MASK));
		ruta = getClass().getResource("/proyecto/rec/escalar.png");
		;
		m2.setIcon(new ImageIcon(ruta));
		m2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cEscalar obj = new cEscalar(v, true, "Define la cantidad a escalar", true);
				float esc = obj.mostrar();
				if (esc != -1)
					F.Escalar(esc, esc);
				repaint();
			}
		});

		m3 = new JMenuItem("Rotar");
		m3.setMnemonic('o');
		m3.setToolTipText("Permite rotarla figura los grados que se especifiquen");
		m3.setAccelerator(KeyStroke.getKeyStroke('O', InputEvent.CTRL_MASK));
		ruta = getClass().getResource("/proyecto/rec/rotar.png");
		;
		m3.setIcon(new ImageIcon(ruta));
		m3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cRotar obj = new cRotar(v, true);
				int v[] = obj.mostrar();
				if (v != null && v[1] != 0) {
					if (v[0] == 1) {
						// Rotar en contra
						F.Rotar(v[1]);
					} else {
						// Rotar en el sentido de las manecillas
						F.Rotar(v[1] * -1);
					}
					repaint();
				}
			}
		});
		m4 = new JMenuItem("Deformar");
		m4.setMnemonic('D');
		m4.setToolTipText("Deforma la figura, valor m�ximo 1");
		m4.setAccelerator(KeyStroke.getKeyStroke('D', InputEvent.CTRL_MASK));
		ruta = getClass().getResource("/proyecto/rec/deformar.png");
		m4.setIcon(new ImageIcon(ruta));
		m4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				cEscalar obj = new cEscalar(v, true, "Dame la cantidad a deformar", false);
				float res = obj.mostrar();
				if (res != -1) {
					F.Deformar(res, res);
					repaint();
				}
			}
		});

		m5 = new JMenuItem("Reflección");
		m5.setMnemonic('F');
		m5.setToolTipText("Realiza el proceso de reflección en los diferentes");
		m5.setAccelerator(KeyStroke.getKeyStroke('F', InputEvent.CTRL_MASK));
		ruta = getClass().getResource("/proyecto/rec/reflexion.png");
		m5.setIcon(new ImageIcon(ruta));
		m5.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				cRefleccion obj = new cRefleccion(v, true);
				int res[] = obj.mostrar();
				if (res != null) {
					F.Escalar(res[0], res[1]);
					repaint();
				}
			}
		});

		m6 = new JMenuItem("Trasladar");
		m6.setMnemonic('T');
		m6.setToolTipText("Permite mover la cantidad que se especifique");
		m6.setAccelerator(KeyStroke.getKeyStroke('T', InputEvent.CTRL_MASK));
		ruta = getClass().getResource("/proyecto/rec/transladar.png");
		m6.setIcon(new ImageIcon(ruta));
		m6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cTrasladar obj = new cTrasladar(v, true);
				int res[] = obj.mostrar();
				if (res != null) {
					F.Trasladar(res[0], res[1]);
					repaint();
				}
			}
		});

		m7 = new JMenuItem("Salir");
		m7.setMnemonic('S');
		m7.setAccelerator(KeyStroke.getKeyStroke('S', InputEvent.CTRL_MASK));
		ruta = getClass().getResource("/proyecto/rec/s.png");
		m7.setIcon(new ImageIcon(ruta));
		m7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);

			}
		});

		op1.add(m1);
		op1.add(m2);
		op1.add(m3);
		op1.add(m4);
		op1.add(m5);
		op1.add(m6);
		op1.addSeparator();
		op1.add(m7);

		vP1 = new JMenuItem("Escalar");
		ruta = getClass().getResource("/proyecto/rec/escalar.png");
		vP1.setIcon(new ImageIcon(ruta));
		vP1.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				new vPrevia(vPfig, 1, v);
				v.setVisible(false);
			}
		});

		vP2 = new JMenuItem("Rotar");
		ruta = getClass().getResource("/proyecto/rec/rotar.png");
		vP2.setIcon(new ImageIcon(ruta));
		vP2.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				new vPrevia(vPfig, 2, v);
				v.setVisible(false);
			}
		});

		vP3 = new JMenuItem("Deformar");
		ruta = getClass().getResource("/proyecto/rec/deformar.png");
		vP3.setIcon(new ImageIcon(ruta));
		vP3.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				new vPrevia(vPfig, 3, v);
				v.setVisible(false);
			}
		});

		vP4 = new JMenuItem("Reflección");
		ruta = getClass().getResource("/proyecto/rec/reflexion.png");
		vP4.setIcon(new ImageIcon(ruta));
		vP4.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				new vPrevia(vPfig, 4, v);
				v.setVisible(false);
			}
		});

		vP5 = new JMenuItem("Trasladar");
		ruta = getClass().getResource("/proyecto/rec/transladar.png");
		vP5.setIcon(new ImageIcon(ruta));
		vP5.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				new vPrevia(vPfig, 5, v);
				v.setVisible(false);
			}
		});

		op2.add(vP1);
		op2.add(vP2);
		op2.add(vP3);
		op2.add(vP4);
		op2.add(vP5);

		ayuda = new JMenuItem("Ayuda");
		ayuda.setToolTipText("Muestra la ayuda");
		ayuda.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F1, 0));
		ayuda.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new ayuda();
			}
		});

		op3.add(ayuda);

	}

	public InterfazT2dAPI(int f[][]) {
		v = new JFrame("Java 2D con AffineTransform");
		v.setSize(800, 600);
		setSize(800, 600);
		v.add(this, BorderLayout.CENTER);
		v.setLocationRelativeTo(null);
		F = new Figura2dAPI(f);
		vPfig = f;
		ponerMenu();
		mover = false;
		addMouseMotionListener(new raton2(this));
		addMouseWheelListener(this);
		addMouseListener(new raton1(this));
		popup = new JPopupMenu();
		restaurar = new JMenuItem("Restaurar");
		popup.add(restaurar);
		// Barra de Herramientas
		barraH = new JToolBar("Operaciones frecuentes", JToolBar.VERTICAL);
		v.add(barraH, BorderLayout.WEST);
		URL ruta = getClass().getResource("/proyecto/rec/restaurar.png");
		a1 = new AbstractAction("", new ImageIcon(ruta)) {
			private static final long serialVersionUID = -6454166177353073228L;

			public void actionPerformed(ActionEvent e) {
				F.Restaurar();
				repaint();
			}
		};

		a1.putValue(Action.SHORT_DESCRIPTION, "Restaurar la figura a las coordenada originales");

		b1 = new JButton(a1);
		barraH.add(b1);
		barraH.setFloatable(false);
		ruta = getClass().getResource("/proyecto/rec/rotarLeft.png");
		a2 = new AbstractAction("", new ImageIcon(ruta)) {
			private static final long serialVersionUID = 3777891264055114856L;

			public void actionPerformed(ActionEvent e) {
				F.Rotar(5);
				repaint();
			}
		};
		a2.putValue(Action.SHORT_DESCRIPTION, "Rota la figura a la izquierda 5 grados");
		b2 = new JButton(a2);
		barraH.add(b2);
		ruta = getClass().getResource("/proyecto/rec/rotarRight.png");
		a3 = new AbstractAction("", new ImageIcon(ruta)) {
			private static final long serialVersionUID = -2429841061784073074L;

			public void actionPerformed(ActionEvent e) {
				F.Rotar(-5);
				repaint();
			}
		};
		a3.putValue(Action.SHORT_DESCRIPTION, "Rota la figura a la derecha 5 grados");
		b3 = new JButton(a3);
		barraH.add(b3);
		ruta = getClass().getResource("/proyecto/rec/s.png");
		a4 = new AbstractAction("", new ImageIcon(ruta)) {
			private static final long serialVersionUID = 7131560757025130861L;

			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		};
		a4.putValue(Action.SHORT_DESCRIPTION, "Salir");
		b4 = new JButton(a4);
		barraH.add(b4);

		restaurar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				F.Restaurar();
				repaint();
				popup.setVisible(false);
			}
		});

		v.setVisible(true);
		v.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		F.Dibujar(g);
	}

	public void muestraMenu(MouseEvent e) {
		// isPopupTrigger() indica si es el evento de raton
		// por defecto en el sistema operativo para mostrar
		// el menu.
		if (e.isPopupTrigger()) {
			popup.setLocation(e.getLocationOnScreen());
			popup.setVisible(true);
		}
	}

	public void ocultaMenu(MouseEvent e) {

		popup.setVisible(false);

	}

	public static void main(String[] args) {
		int figura[][] = { { 192, 63 }, { 228, 59 }, { 228, 59 }, { 242, 66 }, { 242, 66 }, { 207, 71 }, { 207, 71 },
				{ 192, 62 }, { 192, 62 }, { 192, 62 }, { 250, 56 }, { 250, 56 }, { 280, 60 }, { 280, 60 }, { 243, 66 },
				{ 243, 66 }, { 232, 58 }, { 232, 58 }, { 232, 58 }, { 283, 51 }, { 283, 51 }, { 322, 57 }, { 322, 57 },
				{ 289, 61 }, { 289, 61 }, { 253, 55 }, { 253, 55 }, { 253, 55 }, { 282, 51 }, { 282, 51 }, { 343, 58 },
				{ 343, 58 }, { 333, 66 }, { 333, 66 }, { 292, 61 }, { 292, 61 }, { 292, 61 }, { 321, 55 }, { 321, 55 },
				{ 384, 63 }, { 384, 63 }, { 373, 71 }, { 373, 71 }, { 335, 66 }, { 335, 66 }, { 335, 66 }, { 344, 58 },
				{ 344, 58 }, { 384, 62 }, { 384, 62 }, { 368, 76 }, { 368, 76 }, { 323, 76 }, { 323, 76 }, { 323, 76 },
				{ 332, 67 }, { 332, 67 }, { 374, 72 }, { 374, 72 }, { 355, 85 }, { 355, 85 }, { 313, 86 }, { 313, 86 },
				{ 313, 86 }, { 321, 76 }, { 321, 76 }, { 366, 77 }, { 366, 77 }, { 356, 86 }, { 356, 86 }, { 282, 85 },
				{ 282, 85 }, { 282, 85 }, { 267, 76 }, { 267, 76 }, { 311, 75 }, { 312, 76 }, { 301, 85 }, { 301, 85 },
				{ 281, 86 }, { 281, 86 }, { 281, 86 }, { 244, 67 }, { 244, 67 }, { 287, 61 }, { 287, 61 }, { 323, 66 },
				{ 323, 66 }, { 303, 85 }, { 303, 85 }, { 303, 85 }, { 229, 85 }, { 229, 85 }, { 214, 77 }, { 214, 77 },
				{ 259, 75 }, { 259, 75 }, { 279, 85 }, { 279, 85 }, { 279, 85 }, { 231, 87 }, { 231, 87 }, { 204, 71 },
				{ 204, 71 }, { 192, 63 }, { 192, 63 }, { 183, 66 }, { 183, 66 }, { 183, 66 }, { 162, 90 }, { 162, 90 },
				{ 174, 97 }, { 174, 97 }, { 196, 75 }, { 196, 75 }, { 183, 66 }, { 183, 66 }, { 183, 66 }, { 147, 106 },
				{ 147, 106 }, { 148, 128 }, { 148, 128 }, { 169, 105 }, { 169, 105 }, { 156, 97 }, { 156, 97 },
				{ 156, 97 }, { 122, 134 }, { 122, 134 }, { 122, 156 }, { 122, 156 }, { 143, 135 }, { 143, 135 },
				{ 142, 112 }, { 142, 112 }, { 142, 112 }, { 121, 135 }, { 121, 135 }, { 121, 173 }, { 121, 175 },
				{ 142, 164 }, { 142, 164 }, { 143, 138 }, { 143, 138 }, { 143, 138 }, { 122, 161 }, { 122, 161 },
				{ 122, 200 }, { 122, 200 }, { 143, 192 }, { 143, 192 }, { 144, 168 }, { 144, 168 }, { 144, 168 },
				{ 122, 175 }, { 122, 175 }, { 122, 199 }, { 122, 199 }, { 160, 188 }, { 160, 188 }, { 172, 159 },
				{ 172, 159 }, { 172, 159 }, { 149, 167 }, { 149, 167 }, { 149, 189 }, { 149, 189 }, { 159, 189 },
				{ 159, 189 }, { 187, 118 }, { 187, 118 }, { 187, 118 }, { 171, 108 }, { 171, 108 }, { 147, 135 },
				{ 146, 136 }, { 148, 161 }, { 148, 161 }, { 174, 151 }, { 174, 151 }, { 174, 152 }, { 160, 189 },
				{ 160, 189 }, { 187, 176 }, { 187, 176 }, { 199, 146 }, { 199, 146 }, { 177, 154 }, { 177, 154 },
				{ 177, 154 }, { 164, 186 }, { 164, 186 }, { 187, 176 }, { 187, 176 }, { 206, 127 }, { 206, 127 },
				{ 193, 119 }, { 193, 119 }, { 192, 119 }, { 180, 147 }, { 180, 147 }, { 201, 138 }, { 201, 138 },
				{ 221, 90 }, { 221, 90 }, { 206, 82 }, { 206, 82 }, { 206, 82 }, { 195, 109 }, { 195, 109 },
				{ 209, 119 }, { 209, 119 }, { 220, 90 }, { 220, 90 }, { 198, 77 }, { 198, 77 }, { 198, 77 },
				{ 178, 100 }, { 178, 100 }, { 189, 107 }, { 189, 107 }, { 204, 81 }, { 204, 81 }, { 220, 90 },
				{ 220, 90 }, { 220, 90 }, { 188, 177 }, { 188, 177 }, { 123, 199 }, { 123, 199 }, { 120, 207 },
				{ 120, 207 }, { 126, 212 }, { 126, 212 }, { 126, 212 }, { 145, 233 }, { 145, 233 }, { 168, 226 },
				{ 168, 226 }, { 147, 202 }, { 147, 202 }, { 126, 211 }, { 126, 211 }, { 126, 211 }, { 158, 249 },
				{ 158, 249 }, { 193, 254 }, { 193, 254 }, { 174, 232 }, { 174, 232 }, { 150, 239 }, { 150, 239 },
				{ 150, 239 }, { 181, 278 }, { 181, 278 }, { 216, 283 }, { 216, 283 }, { 195, 260 }, { 195, 260 },
				{ 165, 257 }, { 165, 257 }, { 165, 257 }, { 182, 277 }, { 182, 277 }, { 240, 288 }, { 240, 288 },
				{ 241, 270 }, { 241, 270 }, { 210, 263 }, { 210, 263 }, { 210, 263 }, { 223, 283 }, { 223, 283 },
				{ 282, 296 }, { 282, 296 }, { 282, 278 }, { 282, 278 }, { 250, 271 }, { 250, 271 }, { 250, 271 },
				{ 247, 288 }, { 247, 288 }, { 282, 297 }, { 282, 297 }, { 289, 264 }, { 289, 264 }, { 254, 247 },
				{ 254, 247 }, { 254, 247 }, { 253, 264 }, { 253, 264 }, { 286, 273 }, { 286, 273 }, { 291, 239 },
				{ 291, 239 }, { 256, 221 }, { 256, 221 }, { 256, 221 }, { 255, 239 }, { 255, 239 }, { 288, 257 },
				{ 288, 257 }, { 290, 240 }, { 290, 240 }, { 236, 208 }, { 236, 208 }, { 236, 208 }, { 214, 216 },
				{ 214, 217 }, { 247, 235 }, { 247, 235 }, { 250, 217 }, { 250, 217 }, { 189, 185 }, { 189, 185 },
				{ 189, 185 }, { 171, 193 }, { 171, 193 }, { 200, 211 }, { 200, 211 }, { 222, 203 }, { 222, 203 },
				{ 190, 185 }, { 190, 185 }, { 190, 185 }, { 155, 199 }, { 155, 199 }, { 172, 223 }, { 172, 223 },
				{ 193, 214 }, { 193, 214 }, { 164, 196 }, { 164, 196 }, { 163, 196 }, { 155, 200 }, { 155, 200 },
				{ 201, 255 }, { 201, 255 }, { 242, 265 }, { 242, 265 }, { 244, 242 }, { 244, 242 }, { 244, 242 },
				{ 205, 219 }, { 204, 219 }, { 179, 229 }, { 179, 229 }, { 235, 207 }, { 235, 207 }, { 291, 240 },
				{ 291, 240 }, { 291, 240 }, { 282, 295 }, { 282, 295 }, { 286, 299 }, { 286, 299 }, { 295, 297 },
				{ 295, 297 }, { 328, 289 }, { 328, 289 }, { 328, 289 }, { 329, 271 }, { 329, 271 }, { 298, 278 },
				{ 299, 278 }, { 297, 297 }, { 297, 297 }, { 353, 284 }, { 353, 284 }, { 353, 284 }, { 371, 262 },
				{ 371, 262 }, { 341, 268 }, { 341, 268 }, { 339, 287 }, { 339, 287 }, { 392, 277 }, { 392, 277 },
				{ 392, 277 }, { 413, 253 }, { 413, 253 }, { 382, 257 }, { 382, 257 }, { 364, 280 }, { 364, 280 },
				{ 393, 276 }, { 393, 276 }, { 393, 276 }, { 427, 235 }, { 427, 235 }, { 411, 228 }, { 411, 228 },
				{ 392, 251 }, { 392, 251 }, { 416, 248 }, { 416, 248 }, { 416, 248 }, { 455, 206 }, { 455, 206 },
				{ 439, 198 }, { 439, 198 }, { 418, 221 }, { 418, 221 }, { 433, 228 }, { 433, 228 }, { 433, 228 },
				{ 455, 207 }, { 455, 207 }, { 427, 192 }, { 427, 192 }, { 394, 212 }, { 394, 212 }, { 408, 221 },
				{ 408, 221 }, { 408, 221 }, { 433, 196 }, { 433, 196 }, { 403, 184 }, { 403, 184 }, { 372, 201 },
				{ 372, 201 }, { 388, 210 }, { 388, 210 }, { 388, 210 }, { 420, 193 }, { 420, 193 }, { 403, 183 },
				{ 403, 183 }, { 345, 212 }, { 345, 212 }, { 344, 233 }, { 344, 233 }, { 344, 233 }, { 377, 217 },
				{ 377, 217 }, { 359, 207 }, { 359, 207 }, { 302, 238 }, { 302, 238 }, { 302, 256 }, { 302, 256 },
				{ 302, 256 }, { 336, 241 }, { 336, 241 }, { 338, 219 }, { 338, 219 }, { 303, 238 }, { 303, 238 },
				{ 301, 272 }, { 301, 272 }, { 301, 272 }, { 332, 267 }, { 332, 267 }, { 333, 247 }, { 333, 247 },
				{ 302, 263 }, { 302, 263 }, { 297, 296 }, { 297, 296 }, { 297, 296 }, { 393, 278 }, { 393, 278 },
				{ 459, 203 }, { 459, 203 }, { 454, 175 }, { 454, 175 }, { 437, 166 }, { 437, 166 }, { 437, 166 },
				{ 441, 187 }, { 441, 187 }, { 458, 198 }, { 458, 198 }, { 450, 161 }, { 450, 161 }, { 432, 139 },
				{ 432, 139 }, { 432, 139 }, { 435, 160 }, { 435, 160 }, { 452, 170 }, { 452, 170 }, { 444, 135 },
				{ 444, 135 }, { 427, 114 }, { 427, 114 }, { 427, 114 }, { 431, 135 }, { 431, 135 }, { 449, 155 },
				{ 449, 155 }, { 445, 135 }, { 445, 135 }, { 415, 98 }, { 415, 98 }, { 415, 98 }, { 408, 106 },
				{ 408, 106 }, { 427, 130 }, { 427, 130 }, { 426, 112 }, { 426, 112 }, { 392, 68 }, { 392, 68 },
				{ 392, 68 }, { 386, 75 }, { 386, 75 }, { 404, 100 }, { 404, 100 }, { 411, 95 }, { 411, 95 },
				{ 392, 69 }, { 392, 69 }, { 392, 69 }, { 377, 80 }, { 377, 80 }, { 390, 107 }, { 390, 107 },
				{ 401, 101 }, { 401, 101 }, { 380, 79 }, { 380, 79 }, { 380, 79 }, { 366, 91 }, { 365, 91 },
				{ 379, 116 }, { 379, 116 }, { 388, 111 }, { 388, 111 }, { 376, 84 }, { 376, 84 }, { 376, 84 },
				{ 366, 91 }, { 366, 91 }, { 388, 138 }, { 388, 138 }, { 406, 147 }, { 406, 147 }, { 393, 121 },
				{ 393, 121 }, { 393, 121 }, { 382, 126 }, { 382, 126 }, { 406, 177 }, { 406, 177 }, { 424, 184 },
				{ 424, 184 }, { 410, 155 }, { 410, 155 }, { 410, 155 }, { 393, 148 }, { 394, 149 }, { 406, 176 },
				{ 406, 176 }, { 437, 189 }, { 437, 189 }, { 433, 167 }, { 433, 167 }, { 433, 167 }, { 416, 158 },
				{ 416, 158 }, { 426, 185 }, { 426, 185 }, { 438, 189 }, { 438, 189 }, { 429, 137 }, { 429, 137 },
				{ 429, 137 }, { 405, 107 }, { 405, 107 }, { 396, 116 }, { 396, 116 }, { 410, 147 }, { 410, 147 },
				{ 432, 159 }, { 432, 159 }, { 432, 159 }, { 452, 170 }, { 452, 170 }, { 445, 136 }, { 445, 136 },
				{ 390, 65 }, { 390, 65 }, { 384, 63 }, { 384, 63 }, { 384, 63 }, { 357, 86 }, { 357, 86 }, { 313, 85 },
				{ 313, 85 }, { 324, 120 }, { 324, 120 }, { 366, 120 }, { 366, 120 }, { 366, 120 }, { 356, 91 },
				{ 356, 91 }, { 316, 92 }, { 316, 92 }, { 284, 89 }, { 284, 89 }, { 273, 117 }, { 273, 117 },
				{ 273, 117 }, { 313, 118 }, { 313, 118 }, { 304, 92 }, { 304, 92 }, { 230, 90 }, { 230, 90 },
				{ 219, 117 }, { 219, 117 }, { 219, 117 }, { 260, 119 }, { 260, 120 }, { 272, 91 }, { 272, 91 },
				{ 231, 90 }, { 231, 90 }, { 211, 137 }, { 211, 137 }, { 211, 137 }, { 242, 156 }, { 242, 156 },
				{ 257, 129 }, { 257, 129 }, { 216, 127 }, { 216, 127 }, { 197, 175 }, { 197, 175 }, { 197, 175 },
				{ 229, 195 }, { 229, 195 }, { 244, 167 }, { 244, 167 }, { 209, 148 }, { 209, 148 }, { 197, 175 },
				{ 197, 175 }, { 197, 175 }, { 253, 207 }, { 253, 207 }, { 287, 189 }, { 287, 189 }, { 257, 171 },
				{ 257, 171 }, { 241, 198 }, { 241, 198 }, { 241, 198 }, { 296, 232 }, { 296, 232 }, { 332, 215 },
				{ 332, 215 }, { 297, 194 }, { 297, 194 }, { 265, 212 }, { 265, 212 }, { 265, 212 }, { 295, 231 },
				{ 295, 231 }, { 352, 200 }, { 352, 200 }, { 340, 171 }, { 340, 171 }, { 307, 187 }, { 307, 187 },
				{ 307, 187 }, { 338, 208 }, { 338, 208 }, { 396, 178 }, { 396, 178 }, { 385, 149 }, { 385, 149 },
				{ 349, 165 }, { 349, 165 }, { 349, 165 }, { 361, 193 }, { 361, 193 }, { 397, 178 }, { 397, 178 },
				{ 374, 130 }, { 374, 130 }, { 333, 128 }, { 333, 128 }, { 333, 128 }, { 345, 154 }, { 345, 154 },
				{ 379, 141 }, { 379, 141 }, { 356, 93 }, { 356, 93 }, { 316, 93 }, { 316, 93 }, { 316, 93 },
				{ 337, 160 }, { 337, 160 }, { 294, 181 }, { 294, 181 }, { 256, 161 }, { 256, 161 }, { 270, 127 },
				{ 270, 127 }, { 270, 126 }, { 326, 127 }, { 326, 127 }, { 314, 87 }, { 314, 87 }, { 344, 58 },
				{ 344, 58 }, { 384, 63 }, { 384, 63 }, { 384, 63 }, { 392, 68 }, { 392, 68 }, { 445, 134 },
				{ 445, 134 }, { 459, 203 }, { 459, 203 }, { 455, 208 }, { 455, 208 }, { 455, 208 }, { 437, 199 },
				{ 437, 199 }, { 382, 254 }, { 382, 254 }, { 342, 260 }, { 342, 260 }, { 344, 239 }, { 344, 239 },
				{ 344, 239 }, { 382, 217 }, { 382, 217 }, { 404, 230 }

		};
		new InterfazT2dAPI(figura);
	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		int rotacion = e.getWheelRotation();
		if (rotacion < 0) {
			F.Escalar(1.1, 1.1);
		} else {
			F.Escalar(.99, .99);
		}
		repaint();
	}
}
