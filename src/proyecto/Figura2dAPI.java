package proyecto;


import java.awt.*;
import java.awt.geom.*;

public class Figura2dAPI {
	Shape s, original;

	public Figura2dAPI(int f[][]) {
		GeneralPath ruta = new GeneralPath();
		ruta.moveTo(f[0][0], f[0][1]);
		for (int i = 1; i < f.length; i++) {
			ruta.lineTo(f[i][0], f[i][1]);
		}
		s = ruta;
		original = s;
	}

	public void Dibujar(Graphics g) {
		// GradientPaint gp = new GradientPaint((float) s.getBounds().getX(), (float) s.getBounds().getCenterY(),
		// 		Color.RED, (float) s.getBounds().getMaxX(), (float) s.getBounds().getCenterY(), Color.BLUE);

		Graphics2D g2d = (Graphics2D) g;
		// g2d.setPaint(gp);
		g2d.draw(s);
	}

	public void Restaurar(){
		s= original;
	}

	public void Escalar(double Sx, double Sy) {
		AffineTransform at = new AffineTransform();
		at.translate(s.getBounds().getCenterX(), s.getBounds().getCenterY());
		at.scale(Sx, Sy);
		at.translate(-s.getBounds().getCenterX(), -s.getBounds().getCenterY());
		s = at.createTransformedShape(s);

	}

	public void Rotar(int ang) {
		double angr = Math.toRadians(ang);
		AffineTransform at = new AffineTransform();
		at.setToRotation(angr, s.getBounds().getCenterX(), s.getBounds().getCenterY());
		s = at.createTransformedShape(s);
	}

	public void Deformar(double A, double B) {
		AffineTransform at = new AffineTransform();
		at.translate(s.getBounds().getCenterX(), s.getBounds().getCenterY());
		at.shear(A, B);
		at.translate(-s.getBounds().getCenterX(), -s.getBounds().getCenterY());
		s = at.createTransformedShape(s);

	}

	public void Trasladar(int tx, int ty) {
		AffineTransform at = new AffineTransform();
		at.setToTranslation(tx, ty);
		s = at.createTransformedShape(s);
	}
}
