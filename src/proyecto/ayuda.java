package proyecto;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;

public class ayuda extends JFrame {
	private static final long serialVersionUID = 7917227069231703359L;
	JLabel instrucciones;
	String txt = "<html><body>CTRL + R restaura la figura a su forma y tamaño original<br>"
			+ "CTRL + E escala la figura en la cantidad dada<br>" + "CTRL + O rota la figura en el sentido dado<br>"
			+ "CRTL + D deforma la figura recibiendo un valor entre 0.0 y 1<br>"
			+ "CTRL + F refleja la figura al plano x,y o xy<br>"
			+ "CTRL + T desplaza la figura en x,y segun la cantidad dada<br>"
			+"Al hacer doble clic dentro de la figura esta rota en sentido de las manecillas del reloj<br>"
			+"Al hacer doble clic fuera de la figura rota en sentido contrario a las manecillas del reloj<br>"
			+"Al hacer clic dentro de la figura y mantener presionado el boton izquierdo del mouse la figura se translada junto con el puntero de mouse al lugar que se desee<br>"
			+"Al girar la rueda del mouse la figura se hará mas grande o pequeña dependiendo del sentido en que se gire la rueda<br>"
			+"</body></html>";

	public ayuda() {
		instrucciones = new JLabel(txt);
		setTitle("Ayuda");
		setSize(300, 400);
		setResizable(false);
		setLocationRelativeTo(null);
		add(instrucciones, BorderLayout.CENTER);
		setVisible(true);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}

	public static void main(String[] args) {
		new ayuda();
	}

}
