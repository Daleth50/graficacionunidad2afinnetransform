package proyecto;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.event.WindowEvent;
import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class vPrevia extends JPanel {

    private static final long serialVersionUID = -4627670815716470435L;
    JFrame Vent;
    Shape s;
    int cont1 = 0, cont2 = 0;
    int coords[][] = { { 10, -10 }, { 10, 0 }, { 0, 10 }, { 0, -10 }, { -10, 0 } };
    String tittle = "Vista previa ";
    public vPrevia(int f[][], int transform, JFrame main) {
        if (transform == 1) {
            tittle += "escalar";
        }
        if (transform == 2) {
            tittle += "rotar";
        }
        if (transform == 3) {
            tittle += "deformar";
        }
        if (transform == 4) {
            tittle += "reflección";
        }
        if (transform == 5) {
            tittle += "transladar";
        }
        Vent = new JFrame(tittle);
        Vent.setSize(600, 400);
        setSize(600, 400);
        Vent.setResizable(false);
        Vent.setLocationRelativeTo(null);
        Vent.add(this, BorderLayout.CENTER);
        GeneralPath ruta = new GeneralPath();
		ruta.moveTo(f[0][0], f[0][1]);
		for (int i = 1; i < f.length; i++) {
			ruta.lineTo(f[i][0], f[i][1]);
		}
		s = ruta;
        Runnable ejecutable = new Runnable() {
            public void run() {
                try {
                    Escalar(.5, .5);
                    repaint();
                    while (true) {

                        Thread.sleep(1000);

                        if (cont1 == 6) {
                            if (transform == 1) {
                            	Escalar(.98, .98);
                                cont2++;
                                if (cont2 == 6) {
                                    cont1 = 0;
                                }
                            }
                            if (transform == 2) {
                            	Rotar(15);
                                cont2++;
                                if (cont2 == 6) {
                                    cont1 = 0;
                                }
                            }
                            if (transform == 3) {
                            	Deformar(.2,.2);
                                cont2++;
                                if (cont2 == 6) {
                                    cont1 = 0;
                                }
                            }
                            if (transform == 4) {
                                Random num = new Random();
                                //F.refleccionH(num.nextBoolean() ? 1 : -1, num.nextBoolean() ? 1 : -1);
                                cont2++;
                                if (cont2 == 6) {
                                    cont1 = 0;
                                }
                            }
                            if (transform == 5) {

                                int num = (int) (Math.random() * 5);
                                Trasladar(coords[num][0], coords[num][1]);
                                cont2++;
                                if (cont2 == 6) {
                                    cont1 = 0;
                                }
                            }

                        } else {
                            if (transform == 1) {
                            	Escalar(1.05, 1.05);
                                cont2 = 0;
                                cont1++;
                            }
                            if (transform == 2) {
                            	Rotar(-15);
                                cont2 = 0;
                                cont1++;
                            }
                            if (transform == 3) {
                            	Deformar(-.2,-.2);
                                cont2 = 0;
                                cont1++;
                            }
                            if (transform == 4) {
                                Random num = new Random();
                                //F.refleccionH(num.nextBoolean() ? 1 : -1, num.nextBoolean() ? 1 : -1);
                                cont2 = 0;
                                cont1++;
                            }
                            if (transform == 5) {

                                int num = (int) (Math.random() * 5);
                                Trasladar(coords[num][0], coords[num][1]);
                                cont2 = 0;
                                cont1++;
                            }

                        }
                        repaint();
                    }

                } catch (Exception e) {

                    e.printStackTrace();

                }

            }
        };
        Thread tarea = new Thread(ejecutable);
        tarea.start();
        Vent.addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosed(java.awt.event.WindowEvent windowEvent) {

                tarea.interrupt();
               // F.restaurarVP();
                main.setVisible(true);
            }
           
        });
        Vent.setVisible(true);
        Vent.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;
        g2d.draw(s);
    }
    
    public void Escalar(double Sx, double Sy) {
		AffineTransform at = new AffineTransform();
		at.translate(s.getBounds().getCenterX(), s.getBounds().getCenterY());
		at.scale(Sx, Sy);
		at.translate(-s.getBounds().getCenterX(), -s.getBounds().getCenterY());
		s = at.createTransformedShape(s);

	}

	public void Rotar(int ang) {
		double angr = Math.toRadians(ang);
		AffineTransform at = new AffineTransform();
		at.setToRotation(angr, s.getBounds().getCenterX(), s.getBounds().getCenterY());
		s = at.createTransformedShape(s);
	}

	public void Deformar(double A, double B) {
		AffineTransform at = new AffineTransform();
		at.translate(s.getBounds().getCenterX(), s.getBounds().getCenterY());
		at.shear(A, B);
		at.translate(-s.getBounds().getCenterX(), -s.getBounds().getCenterY());
		s = at.createTransformedShape(s);

	}

	public void Trasladar(int tx, int ty) {
		AffineTransform at = new AffineTransform();
		at.setToTranslation(tx, ty);
		s = at.createTransformedShape(s);
	}
}